<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
          <style type="text/css">
            .uno{
                line-height: 100px;
                margin: 100px auto;
                text-align: center;
                border: 2px black solid;
                width: 100px;
                height: 100px;
                font-size: 4em;
            }
            </style>
    </head>
    <body>
        <?php
       /** al poner esto sale estos indices para que comentes los pasos que sigues
        * funcion que calcula una potencia
        * @param numero $base
        * @param numero $n
        * @return numero
        */
       function potencia($base,$n){
           //variables locales $p y $i
           $p=1;
           for($i=1;$i<=$n;$i++){
               $p=$p*$base;
           }
           return $p;
       }
       //variables globales
       $b=2;
       $e=5;
       $r=potencia($b,$e);
        ?>
        <div class="uno">
            <?=$b?>
        </div>
        <div class="uno">
            <?=$e?>
        </div>
        <div class="uno">
            <?=$r?>
        </div>
    </body>
</html>
